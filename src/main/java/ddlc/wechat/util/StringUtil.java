package ddlc.wechat.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by hook on 2017/2/17.
 * <p>
 * 字符串工具类
 */
public class StringUtil {

    // 字符串是否为空
    public static boolean isBlank(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    // 是否所有的字符串为空
    public static boolean isAllBlank(CharSequence... css) {
        for (CharSequence cs : css) {
            if (!isBlank(cs)) {
                return false;
            }
        }

        return true;
    }

    // 是否有字符串为空
    public static boolean isHaveBlank(CharSequence... css) {
        for (CharSequence cs : css) {
            if (isBlank(cs)) {
                return true;
            }
        }

        return false;
    }

    // 产生一个随机数
    public static String getRandomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }

        return sb.toString();
    }

    public static String encodeByMD5(String text) {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

        char[] charArray = text.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuilder hexValue = new StringBuilder();
        for (byte md5Byte : md5Bytes) {
            int val = ((int) md5Byte) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    public static String encodeBySHA1(String text) {
        if (null == text || 0 == text.length()) {
            return null;
        }

        return DigestUtils.sha1Hex(text);
    }


    public static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }

        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错??指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }

    public static List<String> getStr(String params) {
        List<String> str = new ArrayList<>();

        if (isBlank(params)) {
            return str;
        }

        String[] param = params.split(",");
        for (String pa : param) {
            if (!isBlank(pa)) {
                str.add(pa);
            }
        }
        return str;
    }

}