package ddlc.wechat.util;

import ddlc.wechat.common.Global;

import java.util.Arrays;

/**
 * Created by  Jun on 2017/6/29.
 * 微信工具类
 */
public class WeChatUtil {


    public static Boolean signature(String signature, String timestamp, String nonce) {
        if (StringUtil.isHaveBlank(signature, timestamp, nonce)) {
            return false;
        }
        String[] contentArray = new String[]{Global.WE_CHAT_TOKEN, timestamp, nonce};
        Arrays.sort(contentArray);
        StringBuffer sb = new StringBuffer();
        for (String content : contentArray) {
            sb.append(content);
        }

        return signature.equals(StringUtil.encodeBySHA1(sb.toString()));
    }
}
