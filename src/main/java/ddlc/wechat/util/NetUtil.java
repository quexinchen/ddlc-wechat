package ddlc.wechat.util;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by  Jun on 2017/6/29.'
 * 网络工具
 */
public class NetUtil {

    private static String ENCODING = "utf-8";

    //httpClient get方法
    public static String get(String url, Map<String, String> paramsMap) {
        CloseableHttpClient client = HttpClients.createDefault();
        String responseText = "";
        CloseableHttpResponse response = null;
        try {
            HttpGet method = new HttpGet(getUrl(url, paramsMap));
            response = client.execute(method);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                responseText = EntityUtils.toString(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseText;
    }

    //httpClient post方法
    public static String post(String url, Map<String, Object> paramsMap) {
        CloseableHttpClient client = HttpClients.createDefault();
        String responseText = "";
        CloseableHttpResponse response = null;
        try {
            HttpPost method = new HttpPost(url);
            if (paramsMap != null) {
                method.setEntity(new UrlEncodedFormEntity(getParamList(paramsMap), ENCODING));
            }
            response = client.execute(method);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                responseText = EntityUtils.toString(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseText;
    }

    private static String getUrl(String url, Map<String, String> params) {
        if (params != null) {
            url = url + "?";
            boolean isFirst = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (!isFirst) {
                    url += "&";
                }
                url += (entry.getKey() + "=" + entry.getValue());
                isFirst = false;
            }
        }

        return url;
    }

    private static List<NameValuePair> getParamList(Map<String, Object> paramsMap) {
        List<NameValuePair> paramList = new ArrayList<>();
        for (Map.Entry<String, Object> param : paramsMap.entrySet()) {
            NameValuePair pair = new BasicNameValuePair(param.getKey(), String.valueOf(param.getValue()));
            paramList.add(pair);
        }

        return paramList;
    }

}
