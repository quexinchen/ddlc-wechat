package ddlc.wechat.controller;

import ddlc.wechat.service.IWeChatService;
import ddlc.wechat.util.WeChatUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by  Jun on 2017/6/27.
 * 微信入口
 */
@RestController
@RequestMapping("/WeChat")
public class WeChatController {
    @Resource
    private IWeChatService weChatService;

    @GetMapping("/callback")
    public void callBack(String signature, String timestamp, String nonce, String echostr, HttpServletResponse resp) {
        if (!WeChatUtil.signature(signature, timestamp, nonce)) {
            return;
        }

        PrintWriter pw = null;
        try {
            pw = resp.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert pw != null;
        pw.append(echostr);
        pw.flush();
    }

}
